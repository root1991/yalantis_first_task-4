package com.example.dwork.csi.models;

import java.util.List;

/**
 * Created by Korzch Alexandr on 3/13/2016.
 */
public class NewsModel {


    private String title;
    private String status;
    private String create_hint;
    private String create_date;
    private String register_hint;
    private String register_date;
    private String solve_hint;
    private String solve_date;
    private String solver_hint;
    private String solver_name;
    private String main_text;

    private List<String> urlList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreate_hint() {
        return create_hint;
    }

    public void setCreate_hint(String create_hint) {
        this.create_hint = create_hint;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getRegister_hint() {
        return register_hint;
    }

    public void setRegister_hint(String register_hint) {
        this.register_hint = register_hint;
    }

    public String getRegister_date() {
        return register_date;
    }

    public void setRegister_date(String register_date) {
        this.register_date = register_date;
    }

    public String getSolve_hint() {
        return solve_hint;
    }

    public void setSolve_hint(String solve_hint) {
        this.solve_hint = solve_hint;
    }

    public String getSolve_date() {
        return solve_date;
    }

    public void setSolve_date(String solve_date) {
        this.solve_date = solve_date;
    }

    public String getSolver_hint() {
        return solver_hint;
    }

    public void setSolver_hint(String solver_hint) {
        this.solver_hint = solver_hint;
    }

    public String getSolver_name() {
        return solver_name;
    }

    public void setSolver_name(String solver_name) {
        this.solver_name = solver_name;
    }

    public String getMain_text() {
        return main_text;
    }

    public void setMain_text(String main_text) {
        this.main_text = main_text;
    }

    public List<String> getUrlList() {
        return urlList;
    }

    public void setUrlList(List<String> urlList) {
        this.urlList = urlList;
    }
}
