package com.example.dwork.csi.services;

import android.content.Context;

import com.example.dwork.csi.R;
import com.example.dwork.csi.models.NewsModel;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import rx.Observable;

/**
 * Created by DWork on 3/13/2016.
 */
public class RequestService {

    Context context;

    public RequestService(Context context) {
        this.context = context;
    }

    //retrofit
    public Observable<NewsModel> getNew(){

        NewsModel newsModel = new NewsModel();

        newsModel.setTitle(context.getResources().getString(R.string.title));
        newsModel.setStatus(context.getResources().getString(R.string.status));
        newsModel.setCreate_hint(context.getResources().getString(R.string.create));
        newsModel.setCreate_date(context.getResources().getString(R.string.create_date));
        newsModel.setRegister_hint(context.getResources().getString(R.string.registr));
        newsModel.setRegister_date(context.getResources().getString(R.string.registr_date));
        newsModel.setSolve_hint(context.getResources().getString(R.string.solv_until));
        newsModel.setSolve_date(context.getResources().getString(R.string.solve_date));
        newsModel.setSolver_hint(context.getResources().getString(R.string.solver));
        newsModel.setSolver_name(context.getResources().getString(R.string.solver_name));
        newsModel.setMain_text(context.getResources().getString(R.string.new_));

        newsModel.setUrlList(Arrays.asList(context.getResources().getStringArray(R.array.images)));

        return Observable.just(newsModel).delay(3, TimeUnit.SECONDS);

    }

}
